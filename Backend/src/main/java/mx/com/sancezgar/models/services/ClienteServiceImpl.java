package mx.com.sancezgar.models.services;

import java.util.List;
import mx.com.sancezgar.models.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import mx.com.sancezgar.models.dao.IClienteDao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteServiceImpl implements IClienteService{

    @Autowired
    private IClienteDao clienteDao;
    
    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return (List<Cliente>)clienteDao.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<Cliente> findAll(Pageable pageable) {
        return clienteDao.findAll(pageable);
    }

    @Override
    @Transactional
    public Cliente save(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    @Transactional
    public void delete(Long idCliente) {
        clienteDao.deleteById(idCliente);
    }

    @Override
    @Transactional(readOnly = true)
    public Cliente findById(Long idCliente) {
        return clienteDao.findById(idCliente).orElse(null);
    }

    
    
}
