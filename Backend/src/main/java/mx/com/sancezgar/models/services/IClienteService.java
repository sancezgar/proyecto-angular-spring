package mx.com.sancezgar.models.services;

import java.util.List;
import mx.com.sancezgar.models.entity.Cliente;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

public interface IClienteService {
    
    public List<Cliente> findAll();
    public Page<Cliente> findAll(Pageable pageable);
    public Cliente save(Cliente cliente);
    public void delete(Long idCliente);
    public Cliente findById(Long idCliente);
    
}
