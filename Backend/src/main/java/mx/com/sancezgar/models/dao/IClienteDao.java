package mx.com.sancezgar.models.dao;

import mx.com.sancezgar.models.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClienteDao extends JpaRepository<Cliente, Long>{
    
}
