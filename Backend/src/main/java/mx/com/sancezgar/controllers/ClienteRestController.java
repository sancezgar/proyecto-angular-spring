package mx.com.sancezgar.controllers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import javax.validation.Valid;
import mx.com.sancezgar.models.services.IClienteService;
import mx.com.sancezgar.models.entity.Cliente;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ClienteRestController {

    @Autowired
    private IClienteService clienteService;
    private final Logger log = LoggerFactory.getLogger(ClienteRestController.class);

    @GetMapping("/clientes")
    public List<Cliente> index() {
        return clienteService.findAll();
    }
    
    @GetMapping("/clientes/page/{page}")
    public Page<Cliente> index(@PathVariable Integer page) {
        Pageable pageable = PageRequest.of(page, 4);
        return clienteService.findAll(pageable);
    }

    @GetMapping("/cliente/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {

        Cliente cliente = null;
        Map<String, Object> response = new HashMap<>();

        try {
            cliente = clienteService.findById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (cliente == null) {
            response.put("mensaje", "El cliente ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
    }

    @PostMapping("/cliente")
    public ResponseEntity<?> create(@Valid @RequestBody Cliente cliente, BindingResult result) {
        Cliente clienteNew = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()) {

            /*List<String> errors = new ArrayList<>();
            
            for(FieldError err:result.getFieldErrors()){
                errors.add("El campo '" + err.getField() + "' " +err.getDefaultMessage());
            }*/
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(
                            e -> "El campo '" + e.getField() + "' " + e.getDefaultMessage()
                    ).collect(Collectors.toList());

            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);

        }

        try {
            clienteNew = clienteService.save(cliente);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al guardar en la base de datos");
            response.put("error", e.getMessage().concat(": ".concat(e.getMostSpecificCause().getMessage())));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El cliente ha sido creado con exito!");
        response.put("cliente", clienteNew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @PutMapping("/cliente/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Cliente cliente, BindingResult result , @PathVariable Long id) {
        Cliente clienteActual = clienteService.findById(id);
        Cliente clienteUpdate = null;

        Map<String, Object> response = new HashMap<>();
        
        if(result.hasErrors()){
            
            List<String> errors = result.getFieldErrors().stream().map(
                    e -> "El campo '" + e.getField() + "' " + e.getDefaultMessage()
            ).collect(Collectors.toList());
            
            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
            
        }
        

        if (clienteActual == null) {
            response.put("mensaje", "Error: no se pudo editar, el cliente ID: ".concat(id.toString().concat(" no existe en la base de datos")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        try {

            clienteActual.setNombre(cliente.getNombre());
            clienteActual.setApellido(cliente.getApellido());
            clienteActual.setEmail(cliente.getEmail());

            clienteUpdate = clienteService.save(clienteActual);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar en la base de datos");
            response.put("error", e.getMessage().concat(": ".concat(e.getMostSpecificCause().getMessage())));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "Se actualizó con exito");
        response.put("cliente", clienteUpdate);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/cliente/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        try {
        	
        	Cliente cliente = clienteService.findById(id);
        	
        	String nombreArchivoAnterior = cliente.getFoto();
        	
        	if(nombreArchivoAnterior != null && nombreArchivoAnterior.length() > 0) {
        		
        		Path rutaFotoAnterior = Paths.get("uploads").resolve(nombreArchivoAnterior).toAbsolutePath();
        		File archivoFotoAnterior = rutaFotoAnterior.toFile();
        		if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
        			archivoFotoAnterior.delete();
        		}
        	}
        	
            clienteService.delete(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Hubo un error al eliminar al cliente en la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getLocalizedMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "Cliente eliminado corretamente");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }
    
    @PostMapping("/clientes/upload")
    public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id){
    	
    	Map<String,Object> response = new HashMap<>();
    	
    	Cliente cliente = clienteService.findById(id);
    	
    	if(!archivo.isEmpty()) {
    		String nombreArchivo = UUID.randomUUID().toString() + "_" +archivo.getOriginalFilename().replace(" ", "_");
    		Path rutaArchivo = Paths.get("uploads").resolve(nombreArchivo).toAbsolutePath();
    		log.info(rutaArchivo.toString());
    		try{
    			Files.copy(archivo.getInputStream(), rutaArchivo);
    		}catch(IOException e) {

    			response.put("mensaje", "Error al subir la imagen del cliente " + nombreArchivo);
    			response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
    			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    		}
    		
    		String nombreFotoAnterior = cliente.getFoto();
    		
    		if(nombreFotoAnterior != null && nombreFotoAnterior.length() > 0 ) {
    			Path rutaFotoAnterior = Paths.get("uploads").resolve(nombreFotoAnterior).toAbsolutePath();
    			File archivoFotoAnterior = rutaFotoAnterior.toFile();
    			if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
    				archivoFotoAnterior.delete();
    			}
    		}
    		
    		cliente.setFoto(nombreArchivo);
    		clienteService.save(cliente);
    		
    		response.put("cliente", cliente);
    		response.put("mensaje", "Has subido correctamente la imagen " + nombreArchivo);
    	}
    	   	
    	
    	return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @GetMapping("/uploads/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
    	
    	Path rutaArchivo = Paths.get("uploads").resolve(nombreFoto).toAbsolutePath();
    	Resource recurso = null;
    	log.info(rutaArchivo.toString());
    	try {
			recurso = new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    	
    	if(!recurso.exists() && !recurso.isReadable()) {
    		throw new RuntimeException("Error no se pudo cargar la imagen: " + nombreFoto);
    	}
    	
    	//Creamos la cabecera para forzar la descarga del archivo
    	HttpHeaders cabecera = new HttpHeaders();
    	cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");
    	
    	return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
    }
    
}
