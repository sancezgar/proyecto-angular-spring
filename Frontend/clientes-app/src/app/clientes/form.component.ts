import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente();
  public titulo: string = "Crear Cliente";
  public errores: string[];

  constructor(private clienteService: ClienteService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  cargarCliente(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id'];
        if (id) {
          this.clienteService.getCliente(id).subscribe(cliente => this.cliente = cliente);
        }
      }
    )
  }

  public create(form): void {
//    if (form.valid) {
      //Si desde el modelo this.cliente no existe idCliente se crea y si existe idCliente se modifica
      this.clienteService.create(this.cliente).subscribe(
        json => {
          this.router.navigate(['clientes'])
          //Si existe idCliente en el modelo this.cliente entonces es una modificacion sino es un cliente nuevo y de ahi depende el mesnaje que se envia
          if (this.cliente.idCliente) {
            Swal.fire('Modifico Cliente', `Cliente ${json.cliente.nombre} ${json.cliente.apellido} modificado con exito`, 'success')
          }
          else {
            Swal.fire('Nuevo Cliente', `Cliente ${json.cliente.nombre} ${json.cliente.apellido} creado con exito`, 'success')
          }

        },
        err => {
          this.errores = err.error.errors as string[];
          console.log('Codigo de error desde el backend: ' + err.status);
          console.log(this.errores);
        }
      );
 //   }
  }

}
