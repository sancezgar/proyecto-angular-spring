import {Cliente}  from './cliente'

export const CLIENTES:Cliente[] = [
    {idCliente: 1, nombre: 'Uriel', apellido: 'Sanchez', email:'sancezgar@gmail.com',createAt: '2017-11-20'},
    {idCliente: 2, nombre: 'Patricia', apellido: 'Garcia', email:'paty1012@hotmail.com',createAt: '2018-12-20'},
    {idCliente: 3, nombre: 'Patricia Stephany', apellido: 'Monzon', email:'fanny_mozong@gmail.com',createAt: '2018-01-02'},
    {idCliente: 4, nombre: 'Maria del Refugio', apellido: 'Garcia', email:'cuquigp@yahoo.com',createAt: '2020-02-14'},
    {idCliente: 5, nombre: 'Emmanuel', apellido: 'Lazos', email:'emma.lazos@gmail.com',createAt: '2019-10-25'},
    {idCliente: 6, nombre: 'Marco', apellido: 'Gonzalez', email:'marcou@gmail.com',createAt: '2019-11-20'},
];