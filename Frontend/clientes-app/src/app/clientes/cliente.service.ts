import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { formatDate, DatePipe } from '@angular/common';



@Injectable()
export class ClienteService {

  private url: String = "http://localhost:8080/api/";
  private httpHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })


  constructor(private http: HttpClient, private router: Router) { }

  getClientes(page:number): Observable<Cliente[]> {

    //return of(CLIENTES);
    //return this.http.get<Cliente[]>(this.url + 'clientes');

    return this.http.get(this.url + 'clientes/page/' + page).pipe(
      tap(
        (response: any) => {
          (response.content as Cliente[]).forEach(cliente => {
            console.log(cliente.nombre);
          })
        }
      ),
      map((response: any) => {

        (response.content as Cliente[]).map(
          cliente => {
            cliente.nombre = cliente.nombre.toUpperCase();
            // let datePipe = new DatePipe('es-MX');
            // cliente.createAt = datePipe.transform(cliente.createAt,'fullDate')//formatDate(cliente.createAt,'dd/MM/yyyy','en-US');
            return cliente;
          }

        );
        return response;
      }),
      tap(
        (response:any)=> {
          console.log('ClientesService: tap 2');
          (response.content as Cliente[]).forEach(cliente => {
            console.log(cliente.nombre);
          })
        }
      ),
    );

  }

  create(cliente: Cliente): Observable<any> {
    return this.http.post<any>(this.url + 'cliente', cliente, { headers: this.httpHeaders }).pipe(
      catchError(e => {

        if (e.status == 400) {
          return throwError(e);
        }

        console.log(e.error.mensaje);
        Swal.fire('Error!', e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  getCliente(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.url}cliente/${id}`).pipe(
      catchError(e => {
        this.router.navigate(['/clientes']);
        console.log(e.error.mensaje);
        Swal.fire('Error al editar!', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Cliente> {
    return this.http.delete<Cliente>(`${this.url}cliente/${id}`, { headers: this.httpHeaders }).pipe(
      catchError(e => {
        console.log(e.error.error);
        Swal.fire('Error!', e.error.error, 'error');
        return throwError(e);
      })
    );
  }
}
