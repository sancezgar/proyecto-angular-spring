import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() paginador: any;

  paginas: number[];

  desde: number;
  hasta: number;


  constructor() { }
  ngOnChanges(changes:SimpleChanges): void {
    let paginadorActualizado = changes['paginador'];
    if(paginadorActualizado.previousValue){
      this.initPaginator();
    }
  }

  ngOnInit(): void {
    this.initPaginator();
  }

  private initPaginator(): void {
    if (this.paginador.totalPages > 5) {
      this.desde = Math.min(Math.max(1, this.paginador.number - 2), this.paginador.totalPages - 4);
      this.hasta = Math.max(Math.min(this.paginador.totalPages, this.paginador.number + 4), 6);

      this.paginas = new Array(this.hasta - this.desde + 1).fill(0).map(
        (valor, indice) => indice + this.desde
      );

    } else {
      this.paginas = new Array(this.paginador.totalPages).fill(0).map(
        (valor, indice) => indice + 1
      );
    }
  }

}
